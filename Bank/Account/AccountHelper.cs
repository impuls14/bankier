﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank.Account
{
    public static class AccountHelper
    {
        public static String GenerateAccountNumber()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
