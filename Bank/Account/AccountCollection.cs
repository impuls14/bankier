﻿using Bank.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    public class AccountCollection
    {
        public AccountCollection()
        {
            accountIdCounter = 0;
            accountList = new List<IAccount>();
        }

        private static Int32 accountIdCounter;

        private List<IAccount> accountList;

        public IAccount GetAccountByID(String accountNumber, Int32 clientID)
        {
            IAccount account = accountList.FirstOrDefault(a => a.number == accountNumber && a.clientId == clientID);
            return account;
        }

        public void CreateCurrentAccount(Int32 clientId)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            CurrentAccount account = new CurrentAccount(clientId, newId);
            accountList.Add(account);

        }
        public void CreateCurrentAccount(Int32 clientId, Decimal ballance)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            CurrentAccount account = new CurrentAccount(clientId, newId, ballance);
            accountList.Add(account);
        }

        public void CreateDepositAccount(Int32 clientId)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            DepositAccount account = new DepositAccount(clientId, newId);
            accountList.Add(account);

        }
        public void CreateDepositAccount(Int32 clientId, Decimal ballance)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            DepositAccount account = new DepositAccount(clientId, newId, ballance);
            accountList.Add(account);
        }

        public void CreateForeignCurrencyAccount(Int32 clientId, Currency cur)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            ForeignCurrencyAccount account = new ForeignCurrencyAccount(clientId, newId, cur);
            accountList.Add(account);

        }
        public void CreateForeignCurrencyAccount(Int32 clientId, Decimal ballance, Currency cur)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            ForeignCurrencyAccount account = new ForeignCurrencyAccount(clientId, newId, cur, ballance);
            accountList.Add(account);
        }

        public void CreateSavingsAccount(Int32 clientId)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            SavingsAccount account = new SavingsAccount(clientId, newId);
            accountList.Add(account);

        }
        public void CreateSavingsAccount(Int32 clientId, Decimal ballance)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            SavingsAccount account = new SavingsAccount(clientId, newId, ballance);
            accountList.Add(account);
        }

        public void CreateSecondaryAccount(Int32 clientId)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            SecondaryAccount account = new SecondaryAccount(clientId, newId);
            accountList.Add(account);

        }
        public void CreateSecondaryAccount(Int32 clientId, Decimal ballance)
        {
            int newId = accountList.Max(a => a.id);
            newId++;
            SecondaryAccount account = new SecondaryAccount(clientId, newId, ballance);
            accountList.Add(account);
        }

        internal IAccount getAccountByID(string accountID)
        {
            return accountList.FirstOrDefault(a => a.number == accountID);
        }
    }
}
