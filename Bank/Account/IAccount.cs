﻿using Bank.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    public interface IAccount
    {
        #region properties

        Decimal ballance
        {
            get;
            set;
        }

        Int32 id
        {
            get;
            set;
        }

        Int32 clientId
        {
            get;
            set;
        }
        
        String number
        {
            get;
            set;
        }


        List<IOperation> operationlist
        {
            get;
            set;
        }

        IInterestCalculator interestCalculator
        {
            get;
            set;
        }

        #endregion

    }
}
