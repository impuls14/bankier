﻿using Bank.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank.Account
{
    class ForeignCurrencyAccount : IAccount
    {
        private Decimal ballance;

        public Decimal Ballance
        {
            get { return ballance; }
            set { ballance = value; }
        }

        private Int32 id;

        public Int32 Id
        {
            get { return id; }
            set { id = value; }
        }

        private Int32 clientId;

        public Int32 ClientId
        {
            get { return clientId; }
            set { clientId = value; }
        }

        private String number;

        public String Number
        {
            get { return number; }
            set { number = value; }
        }

        private Currency currency;

        public Currency Currency
        {
            get { return currency; }
            set { currency = value; }
        }

        private List<IOperation> operationlist;

        public List<IOperation> Operationlist
        {
            get { return operationlist; }
            set { operationlist = value; }
        }

        private IInterestCalculator interestCalculator;

        public IInterestCalculator InterestCalculator
        {
            get { return interestCalculator; }
            set { interestCalculator = value; }
        }


        public ForeignCurrencyAccount(int _clientId, int _id, Currency _currency)
        {

            Id = _id;
            ClientId = _clientId;
            Number = AccountHelper.GenerateAccountNumber();
            Ballance = 0;
            Currency = _currency;
            //TODO zdefiniowana lista operacji dla konta
            //TODO zdefiniowany mechanizm odsetek dla typu konta

        }

        public ForeignCurrencyAccount(int _clientId, int _id, Currency _currency, Decimal _ballance)
        {
            Id = _id;
            ClientId = _clientId;
            Number = AccountHelper.GenerateAccountNumber();
            ballance = _ballance;
            Currency = _currency;
        }

        decimal IAccount.ballance
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAccount.id
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IAccount.clientId
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IAccount.number
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        List<IOperation> IAccount.operationlist
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        IInterestCalculator IAccount.interestCalculator
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}