﻿namespace Bank
{
    enum AccountType
    {
        /// <summary>
        /// Rachunek bieżący
        /// </summary>
        CurrantAccount = 0,
        
        /// <summary>
        /// Rachunek pomocniczy
        /// </summary>
        SecondaryAccount = 1,

        /// <summary>
        /// Rachunek lokaty terminowej
        /// </summary>
        DepositAccount = 2,

        /// <summary>
        /// Rachunek oszczędnościowy
        /// </summary>
        SavingsAccount = 3,

        /// <summary>
        /// Rachunek walutowy
        /// </summary>
        ForeignCurrencyAccount = 4
    }
}