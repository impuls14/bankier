﻿namespace Bank
{
    public enum Currency
    {
        /// <summary>
        /// Polski złoty
        /// </summary>
        PLN = 0,

        /// <summary>
        /// Euro
        /// </summary>
        EUR = 1,

        /// <summary>
        /// Korona czeska
        /// </summary>
        CZK = 2,

        /// <summary>
        /// Rubel
        /// </summary>
        RUB = 3
    }
}