﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank.Operations
{
    public enum OperationType
    {
        Balance,
        InPayment,
        OutPayment
    }
}
