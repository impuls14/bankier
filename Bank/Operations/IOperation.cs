﻿using System;

namespace Bank.Operations
{
    public interface IOperation
    {
        void DoOperation(IAccount account);
    }
}
