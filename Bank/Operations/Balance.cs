﻿using System;
using Bank.Services;

namespace Bank.Operations
{
    public class Balance : Operation
    {
        public Balance() { }

        public Balance(IOperationLogger operationLogger)
        {
            _operationLogger = operationLogger;
        }
        public override void DoOperation(IAccount account)
        {
            base.DoOperation(account);
        }
    }
}
