﻿using System;
using Bank.Services;

namespace Bank.Operations
{
    public abstract class Operation : IOperation
    {
        protected IOperationLogger _operationLogger = new SimpleOperationLogger();

        public virtual void DoOperation(IAccount account)
        {
            _operationLogger.LogOperation();
        }
    }
}
