﻿using System;

namespace Bank.Operations
{
    public interface IInterestCalculator
    {
        long CalculateInterest(IAccount account);
    }
}
