﻿using System;
using Bank.Services;

namespace Bank.Operations
{
    public class OutPayment : Operation
    {
        public OutPayment() { }

        public OutPayment(IOperationLogger operationLogger)
        {
            _operationLogger = operationLogger;
        }

        public override void DoOperation(IAccount account)
        {
            base.DoOperation(account);
        }
    }
}
