﻿using System;
using Bank.Services;

namespace Bank.Operations
{
    public class InPayment : Operation
    {
        public InPayment() { }

        public InPayment(IOperationLogger operationLogger)
        {
            _operationLogger = operationLogger;
        }

        public override void DoOperation(IAccount account)
        {
            base.DoOperation(account);
        }
    }
}
