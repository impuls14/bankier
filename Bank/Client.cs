﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    public class Client
    {

        public Client()
        {
            lastname = "";
            fistname = "";
        }

        public Client(String name, String lastname)
        {
            lastname = name;
            fistname = lastname;
        }

        public Client(String clientID)
        {
            
        }

        public void getFreeID()
        {

            ClientCollections.getUnOccupiedID();
        }

        public void makePayment(double amount)
        {
            //manager.makePayment(amount);    

        }
        public void requestNewAccount()
        {
            // manager.requestNewAccount(); 
        }

        public void getClientFromDataBase(String fistName , String lastName)
        {
            Client c = database.getClient(fistName, lastName);
            Name = c.Name;
            LastName = c.LastName;
        }

        public void getAccountManager()
        {
            //manager = new AccountManager();
            // manager.connectToBank();               
        }

        public Boolean connect()
        {
            //return manager.connectToBank(); 
            return true;
        }

        public void showClientInformation()
        {
            Console.WriteLine("Client Information");
            Console.WriteLine("FirstName: " + Name);
            Console.WriteLine("LastName: " + LastName);
        }


        private String fistname;
        public String Name
        {
            get { return fistname; }
            set { fistname = value; }
        }

        private String lastname;
        public String LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }


        private String clientID;
        public String ClientID
        {
            get { return clientID; }
            set { clientID = value; }
        }

        private long accounts;
        public long Accounts
        {
            get { return accounts; }
            set { accounts = value; }
        }

        private AccountManager manager;
        public AccountManager Manager
        {
            get { return manager; }
            set { manager = value; }
        }


        private ClientCollections database;
        public ClientCollections DataBase
        {
            get { return database; }
            set { database = value; }
        }

        


    }
      
}